import java.util.LinkedList;
import java.util.Queue;

public class Queues {
    public static void main(String[] args) {
        Queue<Integer> q1 = new LinkedList<>(); // 큐를 사용할때는 링크드 리스트를 사용한다 왜? 그럴까?
        q1.offer(1); // 큐에 값을 넣는 방법
        q1.add(6); // 큐에 값을 넣는 방법 인데 링크드 리스트에 기능이다. add는
        q1.add(56);

        int t1 = q1.poll(); // 맨앞에 있는 값을 빼낸다. 변수에 담을 수도 있다. 큐안에 값이 없으면 null를 리턴한다.
        int t2 = q1.remove(); // 맨앞에 있는 값을 빼낸다. 변수에 담을 수도 있다. 링크드 리스트의 기능이다. 하지만 큐안에 값이 없으면 익셉션이 발생한다.
        System.out.println(q1.peek());// 56 맨앞에 있는값을 확인한다 원본에 영향을 주지 않음
        q1.clear(); // 큐 안에 있는 값을 전부 없앤다. 변수에 담을수 없다.
        System.out.println(t1); // 1
        System.out.println(t2); // 6
        System.out.println(q1); // []
        System.out.println(q1.peek()); // null
        System.out.println(q1.poll()); // null
        System.out.println(q1.remove()); // Exception 오류 발생
    }
}
