import java.util.Vector;

public class Vectors {
    public static void main(String[] args) {
        // 1 : 스칼라
        // [2,34,54] : 백터
        // [[1,2],[3,4]] : 행렬
        Vector<Integer> a = new Vector<>(3); // 어레이리스트로 구현한게 백터이기 때문에 미리 크기를 지정해줘야 그나마 빠르게 등록이 된다. 왜?
        a.add(5);
        a.add(6);
        a.add(15); // [5,6,15] 백터
        Vector<Integer> b = new Vector<>(3); // 어레이리스트는 추가할때 원본 데이터를 복사하면서 크기를 크게 복사해서 값을 넣어주고 그걸로 교체하는 방식이기 때문에
        b.add(7);
        b.add(9);
        b.add(4); // [7,9,4]
        System.out.println("더하기: " + getSum(a,b));
        System.out.println("빼기: " + getMSum(a,b));
        System.out.println("곱하기: " + getGubhagi(a,b));
        System.out.println("나누기: " + getNanugi(a,b));
    }

    // 더하기
    private static Vector<Integer> getSum(Vector<Integer> vectorA, Vector<Integer> vectorB) {
        Vector<Integer> result = new Vector<>(vectorA.size());
        result.add(vectorA.get(0) + vectorB.get(0));
        result.add(vectorA.get(1) + vectorB.get(1));
        result.add(vectorA.get(2) + vectorB.get(2));
        return result;
    }

    // 빼기
    private static Vector<Integer> getMSum(Vector<Integer> vectorA, Vector<Integer> vectorB) {
        Vector<Integer> result = new Vector<>(vectorB.size());
        for (int i = 0; i < vectorA.size(); i++) {
            result.add(vectorA.get(i) - vectorB.get(i));
        }
        return result;
    }

    // 곱하기
    private static Vector<Integer> getGubhagi(Vector<Integer> vectorA, Vector<Integer> vectorB) {
        Vector<Integer> result = new Vector<>(vectorB.size());
        for (int i = 0; i < vectorA.size(); i++) {
            result.add(vectorA.get(i) * vectorB.get(i));
        }
        return result;
    }

    // 나누기
    private static Vector<Double> getNanugi(Vector<Integer> vectorA, Vector<Integer> vectorB) {
        Vector<Double> result = new Vector<>(vectorB.size());
        for (int i = 0; i < vectorA.size(); i++) {
            if (vectorA.get(i) != 0 && vectorB.get(i) != 0) {
                result.add((double) vectorA.get(i) / vectorB.get(i));
            } else result.add(Double.NaN);
        }
        return result;
    }
}
