public class HashTableSolution {
    public static void main(String[] args) {
        HashTable line607 = new HashTable(3);
        line607.setData("MMR", "잠자기");
        line607.setData("KBJ", "코테");
        line607.setData("KSH", "네일아트");
        line607.setData("NJE", "강아지산책");
        line607.setData("HSK", "청소");

        HashTable line608 = new HashTable(2);
        line608.setData("KKK", "드라이브");
        line608.setData("EEE", "바닥쓸기");
        line608.setData("YYY", "밥먹기");

        System.out.println(line607.getNodeValue("KBJ"));
        System.out.println(line607.getNodeValue("KKK"));

        System.out.println(line608.getNodeValue("KKK"));
    }
}
