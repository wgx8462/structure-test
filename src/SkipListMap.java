import java.util.concurrent.ConcurrentSkipListMap;

public class SkipListMap {
    public static void main(String[] args) {
        ConcurrentSkipListMap<String, Integer> bbangLikes = new ConcurrentSkipListMap<>();
        bbangLikes.put("배병수", 8934848);
        bbangLikes.put("노지은", 342809712);
        bbangLikes.put("김기범", 1);
        bbangLikes.put("박진수", 0);
        bbangLikes.put("김남호", 999999999);

        bbangLikes.remove("김기범");
        bbangLikes.remove("박진수");

        System.out.println(bbangLikes); // 출력물만 보면 일반 map이랑 다를게 없지만 실제 저장된 용량이 크기 때문에 로그인 같은 속도 빠른게 필요한 거 사용 할때 필요하다.
        System.out.println(bbangLikes.get("김남호"));
    }
}
