import java.util.*;

public class Iterators {
    public static void main(String[] args) {
        /**
         * 값만 있을 경우
         */
        List<String> testList = new LinkedList<>();
        testList.add("홍길동");
        testList.add("박길동");
        testList.add("고길동");

        Iterator<String> iterator = testList.iterator();
        // hasNext, next, remove 만 있다 사이즈가 없다.
        while (iterator.hasNext()) {
            String resultValue = iterator.next();
            System.out.println(resultValue);
        }
        /**
         * 키 밸류 형태 일때
         */
        HashMap<String, String> map = new HashMap<>();
        map.put("홍길동", "똥을 좋아해");
        map.put("박길동", "똥 안 좋아해");
        map.put("오기리", "별로 관심 없어");

        map.values();

        //엔트리셋 사용하는 경우
        Iterator<Map.Entry<String, String>> iterator2 = map.entrySet().iterator(); // 엔트리셋만으로는 조회가 안된다. 입장만 시켜주는 애이기 때문이다.
        while (iterator2.hasNext()) {
            Map.Entry<String, String> item = iterator2.next();
            System.out.println(item.getKey() + " 은 " + item.getValue());
        }

        //키셋 사용하는 경우
        Iterator<String> iterator21 = map.keySet().iterator();
        while (iterator21.hasNext()) {
            String key = iterator21.next();
            String value = map.get(key);
            System.out.println(value);
        }

        // 스트림을 사용한 이유는 엔트리셋은 한번에 주기 때문에 하나씩 주기 위해서는 다른 거를 사용해야 한다 스트림의 경우 하나씩 넘겨준다.
        map.entrySet().stream().forEach(e -> System.out.println(e.getValue()));
    }
}

