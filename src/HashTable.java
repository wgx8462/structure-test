import java.util.LinkedList;
// 이코드는 실전에서 쓰는 코드가 아니다 어떻게 돌아 가는지만 알
public class HashTable { // 해시 테이블의 개선책이 해시맵이다.
    // 자료 구조 예약어가 없다. 옛날게 있긴하다.
    // 클래스 안에 클래스를 만들수 있다.
    private LinkedList<Node>[] list;

    public HashTable(int size) {
        this.list = new LinkedList[size];
    }

    public int getHash(String key) {
        int totalHash = 0;
        for (char c : key.toCharArray()) totalHash += c; // 따로 캐스팅을 하지 않아도 알아서 바꿔서 알파벳을 아스키로드 10진수로 변환해서 넣어준다.
        return totalHash;
    }

    public int getRoomNumber(int hash) {
        return hash % list.length; // 나머지값을 구하는 퍼센트 연산자를 사용해서 나머지를 구한다.
    }

    public Node searchNode(LinkedList<Node> list, String key) {
        if (list == null) return null;
        for (Node node : list) {
            if (node.getKey().equals(key)) return node; // 스트링 비교 왜 ==이 아니라 equals를 사용할까?  ==은 주소값이 같은지 확인하는거다. 문자가이닌 다른 값들은 리터럴형태를 가지고 있다.equals 값을 가지고 와서 비교한다.
        }
        return null;
    }

    public void setData(String key, String value) {
        int hash = getHash(key);
        int roomIndex = getRoomNumber(hash);

        LinkedList<Node> nodes = list[roomIndex];

        if (list[roomIndex] == null) list[roomIndex] = new LinkedList<>();

        Node node = searchNode(nodes, key);
        if (node == null) list[roomIndex].add(new Node(key, value));
        else node.setValue(value);
    }

    public String getNodeValue(String key) {
        int hash = getHash(key);
        int roomIndex = getRoomNumber(hash);
        LinkedList<Node> nodes = list[roomIndex];
        Node node = searchNode(nodes, key);

        return node == null ? "No Data" : node.getValue();
    }

    public static class Node {
        // 생성자를 만들어줘야한다.
        // 클래스는 설계도면을 가지고 우리가 구현을 해야하는데
        // 생서자의 조건은 클래스의 이름과 같아야 한다가 조건이 된다.
        // 실체화를 하기 위해 생성자가 필요하다.
        private String key;
        private String value;
        public Node(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return this.key;
        }

        public String getValue() {
            return this.value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
