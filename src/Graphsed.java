import java.util.*;

public class Graphsed {
    public static void main(String[] args) {
        String[] columnName = {"A", "B", "C", "D", "E", "F"};

        int[][] matrix = {
                {0, 1, 1, 0, 0, 0},
                {1, 0, 0, 1, 1, 0},
                {1, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0},
        };
        bfs(matrix, columnName, "A");
        dfs(matrix, columnName, "A");
    }

    //인접 행렬 방식이다.
    private static void bfs(int[][] matrix, String[] columnName, String start) {
        Queue<String> queue = new LinkedList<>();
        List<String> visit = new ArrayList<>();
        // Arrays.asList의 반환은 ArrayList가 아니다 가짜 ArrayList이다 원소에 null값을 가질 수 있다.
        // 클래스에서 스태틱 처리된 메서드를 불러올수 있는걸 알려주시기 위해 쓰셨다 더 간단하게 구현이 구현이 가능
        int startIndex = Arrays.asList(columnName).indexOf(start);

        queue.offer(columnName[startIndex]);
        visit.add(columnName[startIndex]);

        while (!queue.isEmpty()) {
            String node = queue.poll();
            System.out.println(node);

            int nodeIndex = Arrays.asList(columnName).indexOf(node);
            for (int i = 0; i < matrix[nodeIndex].length; i++) {
                if (matrix[nodeIndex][i] == 1 && !visit.contains(columnName[i])) {
                    queue.offer(columnName[i]);
                    visit.add(columnName[i]);
                }
            }
        }
        System.out.println(visit);
    }

    private static void dfs(int[][] matrix, String[] columnName, String start) {
        Stack<String> stack = new Stack<>();
        List<String> visit = new ArrayList<>();

        stack.push(start);

        while (!stack.isEmpty()) {
            String node = stack.pop();
            System.out.println(node);
            int nodeIndex = Arrays.asList(columnName).indexOf(node);
            visit.add(columnName[nodeIndex]);

            for (int i = 0; i < matrix[nodeIndex].length; i++) {
                if (matrix[nodeIndex][i] == 1 && !visit.contains(columnName[i])) {
                    stack.push(columnName[i]);
                }
            }
        }
        System.out.println(visit);
    }
}
