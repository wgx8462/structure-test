import java.util.*;

public class Graphs {
    public static void main(String[] args) {
        String[] abc = {"A", "B", "C", "D", "E", "F", "G"};
        HashMap<String, LinkedList<String>> graph = new HashMap<>();
        for (String s : abc) graph.putIfAbsent(s, new LinkedList<>());

        LinkedList<String> valueA = graph.get("A");
        valueA.add("B");
        valueA.add("D");

        LinkedList<String> valueB = graph.get("B");
        valueB.add("A");
        valueB.add("F");
        valueB.add("G");

        LinkedList<String> valueC = graph.get("C");
        valueC.add("D");

        LinkedList<String> valueD = graph.get("D");
        valueD.add("A");
        valueD.add("C");

        LinkedList<String> valueE = graph.get("E");
        valueE.add("F");

        LinkedList<String> valueF = graph.get("F");
        valueF.add("B");
        valueF.add("E");

        LinkedList<String> valueG = graph.get("G");
        valueG.add("B");

        bfs(graph, "A");
        dfs(graph, "A");
        dfsRecursion(graph, "A", new ArrayList<>());
    }

    // 인접 리스트 방식이다.
    private static void bfs(HashMap<String, LinkedList<String>> graph, String startKey) {
        // 큐에 담기 위한 하나 만들
        Queue<String> queue = new LinkedList<>();
        // 비교할 임시 변수 만들고
        List<String> visit = new ArrayList<>();

        // 큐에 시작할 스타트키 담고 시작
        queue.offer(startKey);
        // 큐에 담은거 중복이면 안넣기위해 임시변수에 담는다.
        visit.add(startKey);

        // 얼마나 반복할지 모르니까 while문으로 반복시킨다. 근데 그만두는 조건이 큐에 아무것도 없을때이다.
        while (!queue.isEmpty()) {
            // 큐에서 빼면서 빼는 노드의 자식을 담기위한 변수
            String node = queue.poll();

            // 큐에서 빠지는 노드의 자식을 불러오는 변수
            LinkedList<String> neighbor = graph.get(node);
            // 불러온 자식 노드수 만큼 반복시키기 위한 포이치문
            for (String neighborKey : neighbor) {
                // 임시변수에 담아둔 거랑 똑같은게 있으면 담지 않아야하니 비교해서 없을경우 담아준다.
                if (!visit.contains(neighborKey)) {
                    // 큐에 노드를 담는다.
                    queue.offer(neighborKey);
                    // 큐에 담은 노드를 중복으로 담으면 안되니까 임시변수에 비교할수있게 담아준다.
                    visit.add(neighborKey);
                }
            }
        }
        System.out.println(visit);
    }

    private static void dfs(HashMap<String, LinkedList<String>> graph, String start) {
        Stack<String> stack = new Stack<>();
        List<String> visited = new ArrayList<>();
        // 스택은 넣을때 비짓에 넣는게 아니라 팝 할때 넣어야 한다.
        stack.push(start);

        while (!stack.isEmpty()) {
            String node = stack.pop();
            // bfs와 다르게 들어오는 값이 아닌 팝 할때 비짓에 저장을 해줘야 한다.
            visited.add(node);
            LinkedList<String> neighbor = graph.get(node);

            for (String neighborKey : neighbor) {
                if (!visited.contains(neighborKey)) {
                    stack.push(neighborKey);
                }
            }
        }
        System.out.println(visited);
    }

    //모든 재귀가 종료 조건을 언급해야 하는건 아니다.
    private static void dfsRecursion(HashMap<String, LinkedList<String>> graph, String node, List<String> visit) {
        visit.add(node);
        System.out.println(node);

        LinkedList<String> neighbor = graph.get(node);
        for (String neighborKey : neighbor) if (!visit.contains(neighborKey)) dfsRecursion(graph, neighborKey, visit); // 꼬리 재귀 라고 한다. 마지막에 재귀를 부르는 조건이 있다.
    }
}
