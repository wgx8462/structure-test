import java.util.*;

public class Sets {
    public static void main(String[] args) {
        Set<String> ddongLikes = new HashSet<>(); // 해시셋이 셋을 구현한 거기 때문에 문제가 없다.
        ddongLikes.add("문미림");
        ddongLikes.add("문미림"); // 값을 넣을때 순회를 돌고 있어서 안넣었다.
        ddongLikes.add("최연진");
        ddongLikes.add("최재오");
        ddongLikes.add("노지은");

        ddongLikes.remove("최연진");
        ddongLikes.remove("최재오");

        ddongLikes.clear();
        System.out.println(ddongLikes.isEmpty());

        System.out.println(ddongLikes.contains("문미림"));

        Iterator<String> iterator = ddongLikes.iterator();
        while (iterator.hasNext()) { // hasNext는 다음이 있으면 true값을 준다 없으면 false 그래서 값이 아무것도 없어도 에러가 안난다.
            String name = iterator.next();
            System.out.println(name);
        }

        TreeSet<String> treeSet = new TreeSet<>();
        treeSet.add("문미림");
        treeSet.add("김남호");
        treeSet.add("최연진");
        treeSet.add("최재오");
        treeSet.add("강민정");
        treeSet.add("함수빈");
        treeSet.add("김기범");
        treeSet.add("이윤진");
        treeSet.add("홍수경");
        treeSet.add("문미림");
        treeSet.add("문미림");
        treeSet.add("문미림");
        treeSet.add("문미림");
        treeSet.add("문미림");

        System.out.println(treeSet);
        System.out.println(treeSet.first());
        System.out.println(treeSet.last());
        System.out.println(treeSet.lower("문미림"));
        System.out.println(treeSet.higher("문미림"));
        treeSet.pollFirst();
        treeSet.pollLast();
        // 범위 가져오기
        System.out.println(treeSet.subSet("문미림", "최재오"));

        LinkedHashSet<String> nosangbangnyoSet = new LinkedHashSet<>();
        nosangbangnyoSet.add("문미림");
        nosangbangnyoSet.add("문미림");
        nosangbangnyoSet.add("강권김");
        nosangbangnyoSet.add("김김노");

        System.out.println(nosangbangnyoSet);
    }
}
