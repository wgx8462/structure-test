import org.w3c.dom.ls.LSOutput;

import java.util.Stack;

public class Stacks {
    // psvm 으로 만들면 자동으로 만들어준다.
    public static void main(String[] args) {
        // 퍼블릭 공개 스태틱 고정 보이드는 리턴이없다. 메인  스트링배열 아규먼트를 받아야한다.
        // 왜 스트링은 모든걸 포함하기 때문에 왜? 모든 context를 받기위해 환경 흐름 맥락 컨텍스트 변수를 받는다.
        int[] numbers = new int[10];
        numbers[0] = 100;
        numbers[1] = 200;

        double[] ddongGram = {2.2D, 2.2D, 5D}; // 값을 3개 넣고 시작한것이다. {}에 넣어준 이유는 배열은 값을 못바꾸기 때문이다.

        System.out.println(numbers[0]);

        Stack<Integer> stack1 = new Stack<>(); // 스택을 만들었는데 이름은 스택1이다.
        stack1.push(5); // 5를 넣었다.
        stack1.push(4); // 4를 넣었다.
        int p1 = stack1.pop(); // 4를 터트렸다. 이때 p1 변수에 4를 넣어준다. 리턴이있다.
        System.out.println(p1); // p1 변수에 담긴 4를 보여준다.
        stack1.push(6); // 6를 넣었다.
        stack1.clear(); // 전부 없앴다. 리턴이 없다.// [5, 6] 이 나온다.
        stack1.push(1);
        stack1.push(3);
        stack1.push(2);
        stack1.push(4);
        System.out.println(stack1.search(1)); // 4를 찾으면 4가 언제 나오는지 순서를 표시해준다.

        int total = 1;
        int factorial = 8;

        for (int i = 1; i <= factorial; i++) {
            total = total * i;
        }
        System.out.println(total);
        int testValue = fibonacci(8);
        System.out.println(testValue);
    }

    public static int factorial(int n) {
        if (n == 0) return 1;
        else return factorial(n - 1) * n;
    }


    public static int fibonacci(int n) {
        if (n <= 1) return n;
        else return fibonacci(n - 1) + fibonacci(n - 2);
    }
}
