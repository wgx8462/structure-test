import java.util.HashMap;
import java.util.LinkedList;

public class HashMaps {
    public static void main(String[] args) {
        HashMap<String, LinkedList<String>> qna607 = new HashMap<>();
        HashMap<String, LinkedList<String>> qna608 = new HashMap<>();

        qna607.putIfAbsent("DDONG_O", new LinkedList<>()); // put 그냥 넣는데 putIfAbsent는 있으면 안넣고 없으면 넣는다. 그래서 풋이프로 만들어주는게 좋다.
        qna607.putIfAbsent("DDONG_X", new LinkedList<>());
        qna607.putIfAbsent("SIDE_O", new LinkedList<>());
        qna607.putIfAbsent("SIDE_X", new LinkedList<>());

        // 주소값 참조 하는거라 
        qna608.putAll(qna607); // 위에 만들어둔 607꺼를 똑같이 만들어라 라는 뜻이다. HashMap<String, LinkedList<String>> qna608 = new HashMap<>(qna607); 이렇게 처음 만들때부터 대체도 가능하다

        LinkedList<String> dongOStudents = qna607.get("DDONG_O");
        dongOStudents.add("김기범");
        dongOStudents.add("최재오");
        dongOStudents.add("최연진");

        System.out.println(qna607.isEmpty());
        System.out.println(qna607.size());
        System.out.println(dongOStudents.size());
        System.out.println(qna607);
    }
}
