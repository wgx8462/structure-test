import java.util.HashMap;
import java.util.TreeMap;

public class TreeMaps {
    public static void main(String[] args) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("홍길동", "B");
        hashMap.put("박길동", "C");
        hashMap.put("오기리", "D");

        TreeMap<Double, String> sontopMap = new TreeMap<>();
        sontopMap.put(1.34D, "문미림");
        sontopMap.put(1.6D, "최재오");
        sontopMap.put(2D, "배병수");
        sontopMap.put(1.8548373D, "권봉주");

        System.out.println(sontopMap); // 정렬이 되어 보여주는걸 알수있다.
        System.out.println(sontopMap.firstEntry()); // 첫번째
        System.out.println(sontopMap.firstEntry().getKey()); // 첫번째를 가져오는데 키값만
        System.out.println(sontopMap.lastEntry()); // 마지막
        // 트리맵은 키값으로 찾아가면 안된다 O(log n) 으로 찾아가기 때문이다. 이렇게 쓸거면 그냥 해시맵을 써야한다.

        sontopMap.entrySet().stream().forEach(e -> System.out.println(e.getValue()));
    }
}
